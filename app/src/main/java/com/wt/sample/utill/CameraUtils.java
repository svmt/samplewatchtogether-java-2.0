package com.wt.sample.utill;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.util.Size;

import com.wt.sample.data.diagnostic.DiagnosticChild;

import java.util.List;


public class CameraUtils {

    public static void checkVideoResolutions(Context context, List<DiagnosticChild> diagnosticCamera) {
        CameraManager manager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);
        try {
            String[] cameraIds = manager.getCameraIdList();
            for (String id : cameraIds) {
                if (id.equals("2")) {
                    break;
                }

                CameraCharacteristics characteristics = manager.getCameraCharacteristics(id);
                StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);

                boolean isFrontCamera =
                        characteristics.get(CameraCharacteristics.LENS_FACING) == CameraCharacteristics.LENS_FACING_FRONT;
                String cameraName = isFrontCamera ? "Front" : "Back";
                diagnosticCamera.add(new DiagnosticChild(true, "Camera support " + cameraName + " " + id));

                if (map != null) {
                    for (Size size : map.getOutputSizes(SurfaceTexture.class)) {
                        if (size.getWidth() == 320 && size.getHeight() == 240 || size.getWidth() == 640 && size.getHeight() == 480 ||
                                size.getWidth() == 1280 && size.getHeight() == 720 || size.getWidth() == 1920 && size.getHeight() == 1080 ||
                                size.getWidth() == 2560 && size.getHeight() == 1440 || size.getWidth() == 3840 && size.getHeight() == 2160) {
                            diagnosticCamera.add(new DiagnosticChild(true,
                                    "Check resolution size " + size + "(" + size.getHeight() + "p)"));
                        }
                    }
                }
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }
}
