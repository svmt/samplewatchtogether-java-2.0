package com.wt.sample.data.diagnostic;

import java.util.List;

public class DiagnosticParent {

    private String mParent;
    private List<DiagnosticChild> mDiagnosticChildren;

    public DiagnosticParent(String parent, List<DiagnosticChild> diagnosticChildren) {
        this.mParent = parent;
        this.mDiagnosticChildren = diagnosticChildren;
    }

    public String getParent() {
        return mParent;
    }

    public void setParent(String mParent) {
        this.mParent = mParent;
    }

    public List<DiagnosticChild> getDiagnosticChildren() {
        return mDiagnosticChildren;
    }

    public void setDiagnosticChildren(List<DiagnosticChild> mDiagnosticChildren) {
        this.mDiagnosticChildren = mDiagnosticChildren;
    }
}
