package com.wt.sample.data.diagnostic;

import org.jetbrains.annotations.NotNull;

public class ExpandableDiagnosticModel {

    public static final int PARENT = 1;
    public static final int CHILD = 2;

    public DiagnosticParent diagnosticParent = null;
    public DiagnosticChild diagnosticChild = null;
    public int type;
    public boolean isExpanded = false;
    private boolean isCloseShown = false;

    public ExpandableDiagnosticModel(int type, @NotNull DiagnosticParent diagnosticParent,
                                     boolean isExpanded, boolean isCloseShown) {
        this.type = type;
        this.diagnosticParent = diagnosticParent;
        this.isExpanded = isExpanded;
        this.isCloseShown = isCloseShown;
    }

    public ExpandableDiagnosticModel(int type, DiagnosticChild diagnosticChild,
                                     boolean isExpanded, boolean isCloseShown) {
        this.type = type;
        this.diagnosticChild = diagnosticChild;
        this.isExpanded = isExpanded;
        this.isCloseShown = isCloseShown;
    }

    public DiagnosticParent getDiagnosticParent() {
        return diagnosticParent;
    }

    public void setDiagnosticParent(DiagnosticParent diagnosticParent) {
        this.diagnosticParent = diagnosticParent;
    }

    public DiagnosticChild getDiagnosticChild() {
        return diagnosticChild;
    }

    public void setDiagnosticChild(DiagnosticChild diagnosticChild) {
        this.diagnosticChild = diagnosticChild;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }

    public boolean isCloseShown() {
        return isCloseShown;
    }

    public void setCloseShown(boolean closeShown) {
        isCloseShown = closeShown;
    }
}
