package com.wt.sample.data;

public class Message {
  public String id;
  public String message;
  public String name;
  public long date;
  public boolean isLocalMessage;

  public Message(String id, String message, String name, long date, boolean isLocalMessage) {
    this.id = id;
    this.message = message;
    this.name = name;
    this.date = date;
    this.isLocalMessage = isLocalMessage;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public long getDate() {
    return date;
  }

  public void setDate(long date) {
    this.date = date;
  }

  public boolean isLocalMessage() {
    return isLocalMessage;
  }

  public void setLocalMessage(boolean localMessage) {
    isLocalMessage = localMessage;
  }
}
