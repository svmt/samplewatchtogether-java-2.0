package com.wt.sample.data.diagnostic;

import java.util.List;

public class DiagnosticData {

    private List<DiagnosticParent> mDiagnosticParents;

    public DiagnosticData(List<DiagnosticParent> diagnosticParents) {
        this.mDiagnosticParents = diagnosticParents;
    }

    public List<DiagnosticParent> getDiagnosticParents() {
        return mDiagnosticParents;
    }

    public void setDiagnosticParents(List<DiagnosticParent> mDiagnosticParents) {
        this.mDiagnosticParents = mDiagnosticParents;
    }
}
