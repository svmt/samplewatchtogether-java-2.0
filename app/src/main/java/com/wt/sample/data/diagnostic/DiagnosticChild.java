package com.wt.sample.data.diagnostic;

public class DiagnosticChild {

    private boolean mStatus;
    private String mName;

    public DiagnosticChild(boolean status, String name) {
        this.mStatus = status;
        this.mName = name;
    }

    public boolean isStatus() {
        return mStatus;
    }

    public void setStatus(boolean mStatus) {
        this.mStatus = mStatus;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }
}
