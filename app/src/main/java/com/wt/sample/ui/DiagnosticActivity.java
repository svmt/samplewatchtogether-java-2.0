package com.wt.sample.ui;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wt.sample.R;
import com.wt.sample.data.diagnostic.DiagnosticChild;
import com.wt.sample.data.diagnostic.DiagnosticParent;
import com.wt.sample.data.diagnostic.ExpandableDiagnosticModel;
import com.wt.sample.utill.CameraUtils;
import com.wt.sdk.diagnostic.SessionDiagnostic;
import com.wt.sdk.diagnostic.SessionDiagnosticListener;

import org.webrtc.IceCandidate;
import org.webrtc.PeerConnection;
import org.webrtc.PeerConnection.PeerConnectionState;
import org.webrtc.VideoCodecInfo;

import java.util.ArrayList;
import java.util.List;

public class DiagnosticActivity extends AppCompatActivity implements SessionDiagnosticListener {

    private static final String FILE_URL = "PUT_FILE_URL";
    // network/connectivity types
    private static final String NETWORK_TYPE_UDP = "udp";
    private static final String NETWORK_TYPE_TCP = "tcp";
    private static final String NETWORK_TYPE_IPV6 = "ipv6";
    private static final String CONNECTIVITY_TYPE_HOST = "host";
    private static final String CONNECTIVITY_TYPE_RELAY = "relay";
    private static final String CONNECTIVITY_TYPE_REFLEXIVE = "srflx";
    // parent's data
    private static final String PARENT_MICROPHONE = "Microphone";
    private static final String PARENT_CAMERA = "Camera";
    private static final String PARENT_CONNECTIVITY = "Connectivity";
    private static final String PARENT_THROUGHPUT = "Throughput";
    private static final String PARENT_NETWORK = "Network";
    private static final String PARENT_ICE_CANDIDATES_GENERATED = "Ice Candidates Generated";
    private static final String PARENT_ICE_CANDIDATES_RECEIVED = "Ice Candidates Received";
    private static final String PARENT_PEER_CONNECTION_STATES = "Peer Connection States";

    // Session diagnostic
    private SessionDiagnostic mSessionDiagnostic = null;
    private String mDisplayName = "";
    private String mToken = "PUT_TOKEN";
    private String mSessionId = "";

    // diagnostic lists
    private List<ExpandableDiagnosticModel> mExpandableDiagnosticList = new ArrayList<>();
    private List<DiagnosticChild> mDiagnosticIceCandidateGenerated = new ArrayList<>();
    private List<DiagnosticChild> mDiagnosticIceCandidateReceived = new ArrayList<>();
    private List<DiagnosticChild> mDiagnosticNetwork = new ArrayList<>();
    private List<DiagnosticChild> mDiagnosticPeerConnectionStates = new ArrayList<>();

    private DiagnosticExpandableAdapter mDiagnosticExpandableAdapter = null;
    private RecyclerView mRecyclerDiagnostic = null;
    private ContentLoadingProgressBar mProgressDiagnostic = null;

    private Handler mHandler = new Handler(Looper.getMainLooper());

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diagnostic);

        // Extract session's params
        mDisplayName = getIntent().getExtras().getString(StartActivity.DISPLAY_NAME_CODE);

        initSessionDiagnostic();
        initRecyclerDiagnostic(mExpandableDiagnosticList);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSessionDiagnostic.onUnregister();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSessionDiagnostic.onRegister();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSessionDiagnostic.onUnregister();
        mSessionDiagnostic.disconnect();
        mDiagnosticExpandableAdapter.clearAdapter();
    }

    private void initRecyclerDiagnostic(List<ExpandableDiagnosticModel> expandableDiagnosticList) {
        mRecyclerDiagnostic = findViewById(R.id.recycle_diagnostic);
        mProgressDiagnostic = findViewById(R.id.progress_diagnostic);
        mDiagnosticExpandableAdapter = new DiagnosticExpandableAdapter(expandableDiagnosticList);
        mRecyclerDiagnostic.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerDiagnostic.setAdapter(mDiagnosticExpandableAdapter);
        mRecyclerDiagnostic.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }

    private void initSessionDiagnostic() {
        mSessionDiagnostic = new SessionDiagnostic.SessionDiagnosticBuilder()
                .setToken(mToken)
                .setSessionDiagnosticListener(this)
                .build(this);

        mSessionDiagnostic.checkThroughput(FILE_URL);
        mSessionDiagnostic.checkNetworkType();
        mSessionDiagnostic.connect();
    }

    @Override
    public void onConnected(@NonNull String sessionId) {
        this.mSessionId = sessionId;
    }

    @Override
    public void onIceCandidateGenerated(@NonNull IceCandidate iceCandidate) {
        mDiagnosticIceCandidateGenerated.add(new DiagnosticChild(true, iceCandidate.toString()));
    }

    @Override
    public void onIceCandidateReceived(@NonNull IceCandidate iceCandidate) {
        mDiagnosticIceCandidateReceived.add(new DiagnosticChild(true, iceCandidate.toString()));
    }

    @Override
    public void onNetworkType(@NonNull String networkType) {
        mDiagnosticNetwork.add(new DiagnosticChild(true, "Network -> " + networkType));
    }

    @Override
    public void onThroughputData(boolean result, @NonNull String bandwidthQuality, @NonNull String bitsPerSecond) {
        List<DiagnosticChild> diagnosticThroughput = new ArrayList<>();
        diagnosticThroughput.add(new DiagnosticChild(result, "Bandwidth quality " + bandwidthQuality));
        diagnosticThroughput.add(new DiagnosticChild(result, "Bandwidth bits per second " + bitsPerSecond));

        mDiagnosticExpandableAdapter.addDiagnosticChildItemData(
                new ExpandableDiagnosticModel(
                        ExpandableDiagnosticModel.PARENT,
                        new DiagnosticParent(PARENT_THROUGHPUT, diagnosticThroughput),
                        false,
                        true
                )
        );

        mHandler.postDelayed(() -> {
            checkIceCandidate();
            checkNetwork();
            checkConnectivity();
        }, 2000);
    }

    @Override
    public void onWebSocketConnected(boolean b) {
        mHandler.post(() -> {
            checkAudio();
            checkVideo();
        });
    }

    private void checkNetwork() {
        boolean isEnabledUdpProtocol = isEnableNetworkType(NETWORK_TYPE_UDP);
        boolean isEnabledTcpProtocol = isEnableNetworkType(NETWORK_TYPE_TCP);
        boolean isEnabledIpv6Protocol = isEnableNetworkType(NETWORK_TYPE_IPV6);

        mDiagnosticNetwork.add(new DiagnosticChild(isEnabledUdpProtocol, "Udp enabled"));
        mDiagnosticNetwork.add(new DiagnosticChild(isEnabledTcpProtocol, "Tcp enabled"));
        mDiagnosticNetwork.add(new DiagnosticChild(isEnabledIpv6Protocol, "Ipv6 enabled"));

        mDiagnosticExpandableAdapter.addDiagnosticChildItemData(
                new ExpandableDiagnosticModel(
                        ExpandableDiagnosticModel.PARENT,
                        new DiagnosticParent(PARENT_NETWORK, mDiagnosticNetwork),
                        false,
                        true
                )
        );
    }

    private void checkVideo() {
        boolean isCamera = mSessionDiagnostic.isCheckCameraPermission();
        List<DiagnosticChild> diagnosticCamera = new ArrayList<>();
        diagnosticCamera.add(new DiagnosticChild(isCamera, "Check camera permission"));
        VideoCodecInfo[] arrayCodec = mSessionDiagnostic.getVideoCodec();
        for (VideoCodecInfo codec : arrayCodec) {
            diagnosticCamera.add(new DiagnosticChild(true, "Video codec available " + codec.name));
        }

        CameraUtils.checkVideoResolutions(this, diagnosticCamera);

        boolean isCreatedVideoCapture = mSessionDiagnostic.isCreatedVideoCapture();
        diagnosticCamera.add(new DiagnosticChild(isCreatedVideoCapture, "Video capture"));

        mDiagnosticExpandableAdapter.addDiagnosticChildItemData(
                new ExpandableDiagnosticModel(
                        ExpandableDiagnosticModel.PARENT,
                        new DiagnosticParent(PARENT_CAMERA, diagnosticCamera),
                        false,
                        false
                )
        );
    }

    private void checkAudio() {
        List<DiagnosticChild> diagnosticChildren = new ArrayList<>();
        boolean isRecordAudio = mSessionDiagnostic.isCheckRecordAudioPermission();
        diagnosticChildren.add(new DiagnosticChild(isRecordAudio, "Check record audio permission"));

        boolean isCreatedAudioTrack = mSessionDiagnostic.isCreatedAudioTrack();
        diagnosticChildren.add(new DiagnosticChild(isCreatedAudioTrack, "Audio track created using device"));

        mDiagnosticExpandableAdapter.addDiagnosticChildItemData(
                new ExpandableDiagnosticModel(
                        ExpandableDiagnosticModel.PARENT,
                        new DiagnosticParent(PARENT_MICROPHONE, diagnosticChildren),
                        false,
                        true
                )
        );
    }

    private void checkConnectivity() {
        List<DiagnosticChild> diagnosticConnectivity = new ArrayList<>();
        boolean isHostConnectivity = isEnableNetworkType(CONNECTIVITY_TYPE_HOST);
        boolean isRelayConnectivity = isEnableNetworkType(CONNECTIVITY_TYPE_RELAY);
        boolean isReflexiveConnectivity = isEnableNetworkType(CONNECTIVITY_TYPE_REFLEXIVE);

        diagnosticConnectivity.add(new DiagnosticChild(isHostConnectivity, "Host connectivity"));
        diagnosticConnectivity.add(new DiagnosticChild(isRelayConnectivity, "Relay connectivity"));
        diagnosticConnectivity.add(new DiagnosticChild(isReflexiveConnectivity, "Reflexive connectivity"));

        mDiagnosticExpandableAdapter.addDiagnosticChildItemData(
                new ExpandableDiagnosticModel(
                        ExpandableDiagnosticModel.PARENT,
                        new DiagnosticParent(PARENT_CONNECTIVITY, diagnosticConnectivity),
                        false,
                        true
                )
        );

        mProgressDiagnostic.setVisibility(View.GONE);
        mSessionDiagnostic.clearVideoCapture();
    }

    private boolean isEnableNetworkType(String type) {
        for (DiagnosticChild iceCandidate : mDiagnosticIceCandidateGenerated) {
            if (iceCandidate.getName().contains(type)) {
                return true;
            }
        }
        return false;
    }

    private void checkIceCandidate() {
        mDiagnosticExpandableAdapter.addDiagnosticChildItemData(
                new ExpandableDiagnosticModel(
                        ExpandableDiagnosticModel.PARENT,
                        new DiagnosticParent(PARENT_ICE_CANDIDATES_GENERATED, mDiagnosticIceCandidateGenerated),
                        false,
                        true
                )
        );

        mDiagnosticExpandableAdapter.addDiagnosticChildItemData(
                new ExpandableDiagnosticModel(
                        ExpandableDiagnosticModel.PARENT,
                        new DiagnosticParent(PARENT_ICE_CANDIDATES_RECEIVED, mDiagnosticIceCandidateReceived),
                        false,
                        true
                )
        );
    }

    @Override
    public void onPeerConnectionState(@NonNull String state,
        @NonNull PeerConnectionState peerConnectionState) {
        mDiagnosticPeerConnectionStates.add(
            new DiagnosticChild(
                true,
                "State -> $state"
            )
        );

        if (state.equals(PeerConnection.PeerConnectionState.CONNECTED.toString())) {
            mDiagnosticExpandableAdapter.addDiagnosticChildItemData(
                new ExpandableDiagnosticModel(
                    ExpandableDiagnosticModel.PARENT,
                    new DiagnosticParent(
                        PARENT_PEER_CONNECTION_STATES,
                        mDiagnosticPeerConnectionStates
                    ),
                    false,
                    true
                )
            );
        }
    }
}
