package com.wt.sample.ui;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.wt.sample.R;
import com.wt.sample.data.diagnostic.DiagnosticChild;
import com.wt.sample.data.diagnostic.ExpandableDiagnosticModel;

import java.util.List;

public class DiagnosticExpandableAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final List<ExpandableDiagnosticModel> mDiagnosticStateModelList;

    public DiagnosticExpandableAdapter(List<ExpandableDiagnosticModel> diagnosticStateModelList) {
        this.mDiagnosticStateModelList = diagnosticStateModelList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = new ParentViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_expandable_parent, parent, false));
        switch (viewType) {
            case ExpandableDiagnosticModel.PARENT: {
                viewHolder = new ParentViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_expandable_parent, parent, false));
                break;
            }
            case ExpandableDiagnosticModel.CHILD: {
                viewHolder = new ChildViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_expandable_child, parent, false));
                break;
            }
        }
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ExpandableDiagnosticModel row = mDiagnosticStateModelList.get(position);

        if (row.type == ExpandableDiagnosticModel.PARENT) {
            ParentViewHolder parentHolder = ((ParentViewHolder) holder);
            parentHolder.diagnosticName.setText(row.getDiagnosticParent().getParent());
            parentHolder.parentLayout.setOnClickListener(view -> {
                if (row.isExpanded) {
                    collapseRow(position);
                } else {
                    expandRow(position);
                }
                row.isExpanded = !row.isExpanded;
                parentHolder.closeImage.setVisibility(row.isExpanded ? View.GONE : View.VISIBLE);
                parentHolder.upArrowImg.setVisibility(row.isExpanded ? View.VISIBLE : View.GONE);
            });
        } else if (row.type == ExpandableDiagnosticModel.CHILD){
            ChildViewHolder childHolder = ((ChildViewHolder) holder);
            childHolder.stateName.setText(row.diagnosticChild.getName());
            int statusDrawable = row.getDiagnosticChild().isStatus() ? R.drawable.ic_done : R.drawable.ic_failed;
            childHolder.capitalImage.setBackgroundResource(statusDrawable);
        }
    }

    @Override
    public int getItemCount() {
        return mDiagnosticStateModelList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mDiagnosticStateModelList.get(position).type;
    }

    public void addDiagnosticChildItemData(ExpandableDiagnosticModel childItemData) {
        this.mDiagnosticStateModelList.add(childItemData);
        notifyItemInserted(getItemCount() - 1);
    }

    @SuppressLint("NotifyDataSetChanged")
    public void clearAdapter() {
        mDiagnosticStateModelList.clear();
        notifyDataSetChanged();
    }

    @SuppressLint("NotifyDataSetChanged")
    private void expandRow(int position) {
        ExpandableDiagnosticModel row = mDiagnosticStateModelList.get(position);
        int nextPosition = position;
        switch (row.type) {
            case ExpandableDiagnosticModel.PARENT: {
                List<DiagnosticChild> diagnosticChild = row.getDiagnosticParent().getDiagnosticChildren();
                if (diagnosticChild != null) {
                    for (DiagnosticChild child : diagnosticChild) {
                        mDiagnosticStateModelList.add(++nextPosition,
                                new ExpandableDiagnosticModel(ExpandableDiagnosticModel.CHILD, child, false, false)
                        );
                    }
                    notifyDataSetChanged();
                }
                break;
            }
            case ExpandableDiagnosticModel.CHILD: {
                notifyDataSetChanged();
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private void collapseRow(int position) {
        ExpandableDiagnosticModel row = mDiagnosticStateModelList.get(position);
        int nextPosition = position + 1;
        if (row.type == ExpandableDiagnosticModel.PARENT) {
            while (nextPosition != mDiagnosticStateModelList.size() &&
                    mDiagnosticStateModelList.get(nextPosition).type != ExpandableDiagnosticModel.PARENT) {
                mDiagnosticStateModelList.remove(nextPosition);
            }
            notifyDataSetChanged();
        }
    }

    static class ParentViewHolder extends RecyclerView.ViewHolder {
        final ConstraintLayout parentLayout;
        final AppCompatTextView diagnosticName;
        final AppCompatImageView closeImage, upArrowImg;

        ParentViewHolder(View v) {
            super(v);
            parentLayout = v.findViewById(R.id.diagnostic_item_parent_container);
            diagnosticName = v.findViewById(R.id.diagnostic_name);
            closeImage = v.findViewById(R.id.close_arrow);
            upArrowImg = v.findViewById(R.id.up_arrow);
        }
    }

    static class ChildViewHolder extends RecyclerView.ViewHolder {
        final ConstraintLayout childLayout;
        final AppCompatTextView stateName;
        final AppCompatImageView capitalImage;

        ChildViewHolder(View v) {
            super(v);
            childLayout = v.findViewById(R.id.diagnostic_item_child_container);
            stateName = v.findViewById(R.id.txt_diagnostic_name);
            capitalImage = v.findViewById(R.id.iv_status_diagnostic);
        }
    }
}
