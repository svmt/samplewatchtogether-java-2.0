package com.wt.sample.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import com.wt.sample.R;
import com.wt.sdk.VideoRenderer;
import com.wt.sdk.data.participant.LocalParticipant;
import com.wt.sdk.data.participant.OnRemoteParticipantSpeakerListener;
import com.wt.sdk.data.participant.Participant;
import com.wt.sdk.params.MediaConfiguration;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class ParticipantsAdapter extends RecyclerView.Adapter<ParticipantsAdapter.ViewHolder> {

    private final List<Participant> mParticipants = new ArrayList<>();
    private final SwitchCamCallback mSwitchCamCallback;

    public ParticipantsAdapter(final SwitchCamCallback callback) {
        this.mSwitchCamCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, final int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext())
            .inflate(R.layout.item_participant, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {
        final Participant participant = mParticipants.get(position);
        if (participant != null) {
            participant.setRenderer(viewHolder.videoRenderer);
            participant.enableStats();
            participant.setParticipantStatsListener((jsonStats, participant1) -> {
                if (jsonStats != null) {
                    updateStats(viewHolder, jsonStats, participant1);
                }
            });
            participant.enableRemoteAudioSpeaker();
            participant.setRemoteParticipantSpeakerListener(
                new OnRemoteParticipantSpeakerListener() {
                    @Override
                    public void onRemoteParticipantStartSpeaking() {
                        viewHolder.ivActiveSpeaker.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onRemoteParticipantStopSpeaking() {
                        viewHolder.ivActiveSpeaker.setVisibility(View.GONE);
                    }
                });
            viewHolder.mic.setOnClickListener(v -> {
                if (participant.isAudioEnabled()) {
                    participant.disableAudio();
                } else {
                    participant.enableAudio();
                }
                notifyItemChanged(position, participant);
            });
            viewHolder.cam.setOnClickListener(v -> {
                if (participant.isVideoEnabled()) {
                    participant.disableVideo();
                } else {
                    participant.enableVideo();
                }
                notifyItemChanged(position, participant);
            });

            viewHolder.connectionState.setVisibility(participant.isConnectionLost() ? View.VISIBLE : View.GONE);
            viewHolder.layoutProgress.setVisibility(participant.isProgressReconnection() ? View.VISIBLE : View.GONE);
            viewHolder.mic.setBackgroundResource(participant.isAudioEnabled() ? R.drawable.ic_mic_on : R.drawable.ic_mic_off);
            viewHolder.cam.setBackgroundResource(participant.isVideoEnabled() ? R.drawable.ic_video_on : R.drawable.ic_video_off);

            int participantTypeRes;

            switch (participant.getParticipantType()) {
                case VIEWER: {
                    viewHolder.videoRenderer.clearImage();
                    participantTypeRes = R.drawable.ic_viewer;
                    viewHolder.ivParticipantType.setVisibility(View.VISIBLE);
                    break;
                }
                case A_BROADCASTER: {
                    viewHolder.videoRenderer.clearImage();
                    participantTypeRes = R.drawable.ic_microphone;
                    viewHolder.ivParticipantType.setVisibility(View.VISIBLE);
                    break;
                }
                case AV_BROADCASTER:
                case FULL_PARTICIPANT: {
                    participantTypeRes = 0;
                    viewHolder.ivParticipantType.setVisibility(View.GONE);
                    break;
                }
                default:
                    participantTypeRes = 0;
                    viewHolder.ivParticipantType.setVisibility(View.GONE);
                    break;
            }
            viewHolder.ivParticipantType.setImageResource(participantTypeRes);

            viewHolder.name.setText(participant.getName());

            // Check if its local participant
            viewHolder.switchCam.setVisibility(participant instanceof LocalParticipant ? View.VISIBLE : View.GONE);
            viewHolder.volumeLevel.setVisibility(participant instanceof LocalParticipant ? View.GONE : View.VISIBLE);

            // Check if its local participant
            if (position == 0) {
                viewHolder.switchCam.setVisibility(View.VISIBLE);
                viewHolder.volumeLevel.setVisibility(View.GONE);
                viewHolder.switchCam.setOnClickListener(v -> {
                    if (mSwitchCamCallback != null) {
                        mSwitchCamCallback.onSwitchCameraClicked();
                    }
                });
            } else {
                viewHolder.volumeLevel.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if (fromUser) {
                            /*
                             * Sets the volume for the underlying MediaSource. Volume is a gain value in the range
                             * 0 to 10.
                             */
                            participant.setVolumeLevel(progress);
                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return mParticipants.size();
    }

    void remoteParticipantConnectionLost(String participantId) {
        for (int i = 0; i < mParticipants.size(); i++) {
            Participant participant = mParticipants.get(i);
            if (participant.getId().equals(participantId)) {
                int itemIndex = mParticipants.indexOf(participant);
                participant.setConnectionLost(true);
                notifyItemChanged(itemIndex, participant);
            }
        }
    }

    void updateParticipant(String participantId, Participant participant) {
        if (!isExistParticipant(participantId)) {
            addRemoteParticipant(participant);
            return;
        }
        for (int itemIndex = 0; itemIndex < mParticipants.size(); itemIndex++) {
            Participant participantExist = mParticipants.get(itemIndex);
            if (participantExist.getId().equals(participantId)) {
                participantExist.update(participant);
                notifyItemChanged(itemIndex, participantExist);
            }
        }
    }

    void progressConnection(String publisherId, boolean isProgress) {
        for (int itemIndex = 0; itemIndex < mParticipants.size(); itemIndex++) {
            Participant participant = mParticipants.get(itemIndex);
            if (participant.getId().equals(publisherId)) {
                participant.setProgressReconnection(isProgress);
                notifyItemChanged(itemIndex, participant);
            }
        }
    }

    void addLocalParticipant(final Participant participant) {
        if (mParticipants.size() > 0) {
            mParticipants.get(0).update(participant);
            notifyItemChanged(0, participant);
        } else {
            addRemoteParticipant(participant);
        }
    }

    void addRemoteParticipant(final Participant participant) {
        mParticipants.add(participant);
        notifyItemInserted(getItemCount() - 1);
    }

    void removeParticipant(final String participantId) {
        for (int itemIndex = 0; itemIndex < mParticipants.size(); itemIndex++) {
            Participant participant = mParticipants.get(itemIndex);
            if (participant.getId().equals(participantId)) {
                participant.dispose();
                mParticipants.remove(participant);
                notifyItemRemoved(itemIndex);
                break;
            }
        }
    }

    void clearPublishers() {
        for (Participant participant : mParticipants) {
            participant.dispose();
        }
        mParticipants.clear();
        notifyDataSetChanged();
    }

    void updateParticipantMedia(final String participantId, final MediaConfiguration.MediaType mediaType, final MediaConfiguration.MediaState mediaState) {
        if (!mParticipants.isEmpty()) {
            for (int itemIndex = 0; itemIndex < mParticipants.size(); itemIndex++) {
                final Participant participant = mParticipants.get(itemIndex);
                if (participant != null && participant.getId().equals(participantId)) {
                    boolean isEnableMediaState = mediaState == MediaConfiguration.MediaState.ENABLED;
                    switch (mediaType) {
                        case AUDIO:
                            participant.setAudioEnabled(isEnableMediaState);
                            break;
                        case VIDEO:
                            participant.setVideoEnabled(isEnableMediaState);
                            break;
                        default:
                            break;
                    }
                    // Update exactly view after its changed
                    notifyItemChanged(itemIndex, participant);
                    break;
                }
            }
        }
    }

    private Boolean isExistParticipant(String participantId) {
        boolean isExistParticipant = false;
        for (Participant participant : mParticipants) {
            if (participant.getOldParticipantId().equals(participantId) ||
                participant.getId().equals(participantId)) {
                isExistParticipant = true;
            }
        }
        return isExistParticipant;
    }

    String getParticipantName(String participantId) {
        for (Participant participant : mParticipants) {
            if (participant.getId().equals(participantId)) {
                return participant.getName();
            }
        }
        return "";
    }

    private void updateStats(ViewHolder viewHolder, JSONObject jsonStats, Participant participant) {
        int micStateResources;
        try {
            if (jsonStats.get("audioQosMos").toString().equals("EXCELLENT")) {
                if (participant.isAudioEnabled()) {
                    micStateResources = R.drawable.ic_audio_mos_excellent;
                } else {
                    micStateResources = R.drawable.ic_audio_off_mos_excellent;
                }
            } else if (jsonStats.get("audioQosMos").toString().equals("GOOD")) {
                if (participant.isAudioEnabled()) {
                    micStateResources = R.drawable.ic_audio_mos_good;
                } else {
                    micStateResources = R.drawable.ic_audio_off_mos_good;
                }
            } else {
                if (participant.isAudioEnabled()) {
                    micStateResources = R.drawable.ic_audio_mos_bad;
                } else {
                    micStateResources = R.drawable.ic_audio_off_mos_bad;
                }
            }
            viewHolder.mic.setBackgroundResource(micStateResources);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        int camStateResources;
        try {
            if (jsonStats.get("videoQosMos").toString().equals("EXCELLENT")) {
                if (participant.isVideoEnabled()) {
                    camStateResources = R.drawable.ic_video_mos_excellent;
                } else {
                    camStateResources = R.drawable.ic_video_off_mos_excellent;
                }
            } else if (jsonStats.get("videoQosMos").toString().equals("GOOD")) {
                if (participant.isVideoEnabled()) {
                    camStateResources = R.drawable.ic_video_mos_good;
                } else {
                    camStateResources = R.drawable.ic_video_off_mos_good;
                }
            } else {
                if (participant.isVideoEnabled()) {
                    camStateResources = R.drawable.ic_video_mos_bad;
                } else {
                    camStateResources = R.drawable.ic_video_off_mos_bad;
                }
            }
            viewHolder.cam.setBackgroundResource(camStateResources);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    interface SwitchCamCallback {
        void onSwitchCameraClicked();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        final LinearLayout layoutProgress;
        final VideoRenderer videoRenderer;
        final AppCompatTextView name, connectionState;
        final AppCompatImageView mic, cam, switchCam, ivParticipantType, ivActiveSpeaker;
        final SeekBar volumeLevel;

        ViewHolder(View v) {
            super(v);
            layoutProgress = v.findViewById(R.id.layout_progress);
            ivParticipantType = v.findViewById(R.id.iv_participant_type);
            videoRenderer = v.findViewById(R.id.video_renderer);
            ivActiveSpeaker = v.findViewById(R.id.iv_active_speaker);
            name = v.findViewById(R.id.participant_name);
            connectionState = v.findViewById(R.id.txt_connection_state);
            mic = v.findViewById(R.id.mic);
            cam = v.findViewById(R.id.cam);
            switchCam = v.findViewById(R.id.switch_cam);
            volumeLevel = v.findViewById(R.id.volume_level);
        }
    }
}