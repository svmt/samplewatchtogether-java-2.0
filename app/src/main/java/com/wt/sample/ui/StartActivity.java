package com.wt.sample.ui;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.inputmethod.EditorInfo;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import com.wt.sample.BuildConfig;
import com.wt.sample.R;
import com.wt.sample.utill.Utill;

public class StartActivity extends AppCompatActivity {

  public static final String DISPLAY_NAME_CODE = "display_name_code";

  public static final String TYPE_LOGIN = "login";
  public static final String TYPE_DIAGNOSTIC = "diagnostic";

  private AppCompatEditText mDisplayName;

  private String classType = TYPE_LOGIN;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_start);

    ((AppCompatTextView) findViewById(R.id.app_version)).setText(BuildConfig.VERSION_NAME);

    SharedPreferences sharedPref = getSharedPreferences(
        getString(R.string.preference_file_key), Context.MODE_PRIVATE);

    mDisplayName = findViewById(R.id.display_name);
    mDisplayName.setText(sharedPref.getString(DISPLAY_NAME_CODE, ""));

    // Click Listener for sign in button
    findViewById(R.id.sign_in).setOnClickListener(v -> checkMediaPermissions(TYPE_LOGIN));
    findViewById(R.id.diagnostic).setOnClickListener(v -> checkMediaPermissions(TYPE_DIAGNOSTIC));

    // Done actions sign in button click
    mDisplayName.setOnEditorActionListener((v, actionId, event) -> {
      if (actionId == EditorInfo.IME_ACTION_DONE) {
        checkMediaPermissions(TYPE_LOGIN);
        return true;
      }
      return false;
    });
  }

  private void checkMediaPermissions(String type) {
    if (ContextCompat.checkSelfPermission(this, RECORD_AUDIO) + ContextCompat.checkSelfPermission(this, CAMERA) != PERMISSION_GRANTED) {
      ActivityCompat.requestPermissions(this, new String[]{RECORD_AUDIO, CAMERA}, 1);
      return;
    }
    // Permissions granted
    signIn(type);
  }

  // Check if user granted media permissions for app
  @Override
  public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    if (requestCode == 1 && grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[0] == grantResults[1]) {
      signIn(classType);
      return;
    }
    // Permissions not allowed
    onError("Camera and Microphone permissions not allowed");
  }

  private void signIn(String type) {
    final String displayName = String.valueOf(mDisplayName.getText());

    SharedPreferences sharedPref = getSharedPreferences(
        getString(R.string.preference_file_key), Context.MODE_PRIVATE);

    sharedPref.edit()
        .putString(DISPLAY_NAME_CODE, displayName)
        .apply();

    startMainActivity(type, displayName);
  }

  private void startMainActivity(String type, String displayName) {
    final Intent intent = new Intent(StartActivity.this,
            (type.equals(TYPE_LOGIN)) ? MainActivity.class : DiagnosticActivity.class);
    intent.putExtra(DISPLAY_NAME_CODE, displayName);
    startActivity(intent);
  }

  private void onError(final String error) {
    new Handler().post(() -> Utill.showError(StartActivity.this, error));
  }
}
