package com.wt.sample.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import com.wt.sample.R;
import com.wt.sample.data.Message;
import com.wt.sample.utill.Utill;
import java.util.ArrayList;
import java.util.List;
import org.jetbrains.annotations.NotNull;

public class MessengerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private final List<Message> mMessages = new ArrayList<>();

  @NonNull
  @NotNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
    ViewHolder viewHolder;
    if (viewType == 1) {
      viewHolder = new MessengerLocalViewHolder(LayoutInflater.from(parent.getContext())
          .inflate(R.layout.item_messenger_local, parent, false));
    } else {
      viewHolder = new MessengerRemoteViewHolder(LayoutInflater.from(parent.getContext())
          .inflate(R.layout.item_messenger_remote, parent, false));
    }
    return viewHolder;
  }

  @Override
  public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
    Message message = mMessages.get(position);
    String date = Utill.getDate(message.date);

    if (holder instanceof MessengerLocalViewHolder) {
      MessengerLocalViewHolder localViewHolder = ((MessengerLocalViewHolder) holder);
      localViewHolder.name.setText(message.getName());
      localViewHolder.message.setText(message.getMessage());
      localViewHolder.date.setText(date);
    } else if (holder instanceof MessengerRemoteViewHolder) {
      MessengerRemoteViewHolder remoteViewHolder = ((MessengerRemoteViewHolder) holder);
      remoteViewHolder.name.setText(message.getName());
      remoteViewHolder.message.setText(message.getMessage());
      remoteViewHolder.date.setText(date);
    }
  }

  @Override
  public int getItemCount() {
    return mMessages.size();
  }

  @Override
  public int getItemViewType(int position) {
    Message message = mMessages.get(position);
    return message.isLocalMessage ? 1 : 0;
  }

  public void addMessage(Message message) {
    mMessages.add(message);
    notifyItemInserted(mMessages.size() - 1);
  }

  static class MessengerLocalViewHolder extends RecyclerView.ViewHolder {
    final AppCompatTextView name, message, date;

    MessengerLocalViewHolder(View v) {
      super(v);
      name = v.findViewById(R.id.txt_local_name);
      message = v.findViewById(R.id.txt_local_message);
      date = v.findViewById(R.id.txt_local_date);
    }
  }

  static class MessengerRemoteViewHolder extends RecyclerView.ViewHolder {
    final AppCompatTextView name, message, date;

    MessengerRemoteViewHolder(View v) {
      super(v);
      name = v.findViewById(R.id.txt_remote_name);
      message = v.findViewById(R.id.txt_remote_message);
      date = v.findViewById(R.id.txt_remote_date);
    }
  }
}
