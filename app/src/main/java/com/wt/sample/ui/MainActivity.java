package com.wt.sample.ui;

import android.content.res.Configuration;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.wt.sample.R;
import com.wt.sample.data.Message;
import com.wt.sample.utill.Utill;
import com.wt.sdk.Session;
import com.wt.sdk.SessionConnectionListener;
import com.wt.sdk.SessionError;
import com.wt.sdk.SessionListener;
import com.wt.sdk.SessionReconnectListener;
import com.wt.sdk.data.participant.Participant;
import com.wt.sdk.data.participant.ParticipantType;
import com.wt.sdk.params.MediaConfiguration.MediaState;
import com.wt.sdk.params.MediaConfiguration.MediaType;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import org.jetbrains.annotations.NotNull;

public class MainActivity extends AppCompatActivity implements SessionListener,
    SessionReconnectListener, SessionConnectionListener, ParticipantsAdapter.SwitchCamCallback {

    // Const string keys to store values
    private static final String SESSION_CONNECTION_KEY = "session_connection_key";
    private static final String PARTICIPANT_TYPE_KEY = "participant_type";
    // Bundle object to save and get values
    private final Bundle mBundle = new Bundle();

    private Session mSession;
    private String mDisplayName = "";
    private final String mToken = "PUT_TOKEN";

    private ParticipantsAdapter mParticipantAdapter;
    private AppCompatTextView mConnectionState;
    private LinearLayout mParticipantsTypesSheet;

    private Timer mReconnectTimer = null;
    private final Timer mCountTimer = new Timer("MainActivityCountThread");
    private final Timer mConnectionStateTimer = new Timer("MainActivityCountThread");

    private MessengerAdapter mMessengerAdapter = new MessengerAdapter();
    private RecyclerView mRecyclerMessages = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // To prevent screen to be switched off
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        // Extract session's params
        if (getIntent() != null) {
            if (getIntent().getExtras() != null) {
                mDisplayName = String.valueOf(getIntent().getExtras().get(StartActivity.DISPLAY_NAME_CODE));
            }
        }

        initViewsAndListeners();
        initSession();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        RecyclerView recyclerParticipants = findViewById(R.id.participants);
        boolean isPortraitOrientation = newConfig.orientation == Configuration.ORIENTATION_PORTRAIT;
        if (recyclerParticipants.getLayoutManager() instanceof GridLayoutManager) {
            ((GridLayoutManager) recyclerParticipants.getLayoutManager())
                .setSpanCount(isPortraitOrientation ? 2 : 4);
        }
    }

    private void initViewsAndListeners() {
        final AppCompatButton btnStartCameraPreview = findViewById(R.id.btn_cam_preview);
        final AppCompatButton btnConnectSessionFP = findViewById(R.id.btn_connect_session_fp);
        final AppCompatButton btnConnectSessionAVB = findViewById(R.id.btn_connect_session_avb);
        final AppCompatButton btnConnectSessionAB = findViewById(R.id.btn_connect_session_ab);
        final AppCompatButton btnConnectWatching = findViewById(R.id.btn_connect_watching);
        final AppCompatButton btnAdjustVideoQuality = findViewById(R.id.btn_adjust_frame_rate);
        final AppCompatButton btnFPType = findViewById(R.id.btn_pt_fp);
        final AppCompatButton btnAVBType = findViewById(R.id.btn_pt_avb);
        final AppCompatButton btnABType = findViewById(R.id.btn_pt_ab);
        final AppCompatButton btnViewerType = findViewById(R.id.btn_pt_v);
        final AppCompatTextView txtWidth = findViewById(R.id.txt_width_value);
        final AppCompatTextView txtHeight = findViewById(R.id.txt_height_value);
        final AppCompatTextView txtFrameRate = findViewById(R.id.txt_frame_rate_value);
        mParticipantsTypesSheet = this.findViewById(R.id.participants_types_sheet);
        mConnectionState = findViewById(R.id.txt_connection_state);
        final SeekBar widthSeek = findViewById(R.id.seek_width);
        final SeekBar heightSeek = findViewById(R.id.seek_height);
        final SeekBar frameSeek = findViewById(R.id.seek_frame_rate);
        setParticipantsTypesSheet();
        txtWidth.setText(String.valueOf(widthSeek.getProgress()));
        txtHeight.setText(String.valueOf(heightSeek.getProgress()));
        txtFrameRate.setText(String.valueOf(frameSeek.getProgress()));
        // chat messages views
        AppCompatEditText editMessage = findViewById(R.id.edit_message);
        AppCompatImageView btnSendMessage = findViewById(R.id.btn_send_message);
        mRecyclerMessages = findViewById(R.id.recycler_messages);
        mRecyclerMessages.setAdapter(mMessengerAdapter);

        btnStartCameraPreview.setOnClickListener(v -> {
            if (mSession != null) {
                btnStartCameraPreview.setVisibility(View.GONE);
                findViewById(R.id.adjust_media_quality_container).setVisibility(View.VISIBLE);
                mSession.setVideoResolution(widthSeek.getProgress(), heightSeek.getProgress());
                mSession.setVideoFrameRate(frameSeek.getProgress());
                mSession.startCameraPreview();
            }
        });

        btnConnectSessionFP.setOnClickListener(v -> {
            if (mSession != null) {
                findViewById(R.id.controls).setVisibility(View.GONE);
                findViewById(R.id.adjust_media_quality_container).setVisibility(View.VISIBLE);
                setSessionParams();
                mSession.setVideoResolution(widthSeek.getProgress(), heightSeek.getProgress());
                mSession.setVideoFrameRate(frameSeek.getProgress());
                mSession.connect(mToken);
                // save participant type to bundle object
                mBundle.putString(PARTICIPANT_TYPE_KEY, ParticipantType.FULL_PARTICIPANT.name());
            }
        });

        btnConnectSessionAVB.setOnClickListener(v -> {
            if (mSession != null) {
                findViewById(R.id.adjust_media_quality_container).setVisibility(View.VISIBLE);
                findViewById(R.id.controls).setVisibility(View.GONE);
                setSessionParams();
                mSession.setVideoResolution(widthSeek.getProgress(), heightSeek.getProgress());
                mSession.setVideoFrameRate(frameSeek.getProgress());
                mSession.connect(mToken, ParticipantType.AV_BROADCASTER);
                mBundle.putString(PARTICIPANT_TYPE_KEY, ParticipantType.AV_BROADCASTER.name());
            }
        });

        btnConnectSessionAB.setOnClickListener(v -> {
            if (mSession != null) {
                findViewById(R.id.adjust_media_quality_container).setVisibility(View.VISIBLE);
                findViewById(R.id.controls).setVisibility(View.GONE);
                setSessionParams();
                mSession.connect(mToken, ParticipantType.A_BROADCASTER);
                mBundle.putString(PARTICIPANT_TYPE_KEY, ParticipantType.A_BROADCASTER.name());
            }
        });


        btnConnectWatching.setOnClickListener(view -> {
            if (mSession != null) {
                findViewById(R.id.adjust_media_quality_container).setVisibility(View.VISIBLE);
                findViewById(R.id.controls).setVisibility(View.GONE);
                setSessionParams();
                mSession.connect(mToken, ParticipantType.VIEWER);
                mBundle.putString(PARTICIPANT_TYPE_KEY, ParticipantType.VIEWER.name());
            }
        });

        btnFPType.setOnClickListener(view -> {
            if (mSession != null) {
                mSession.setLocalParticipantType(ParticipantType.FULL_PARTICIPANT);
                mBundle.putString(PARTICIPANT_TYPE_KEY, ParticipantType.FULL_PARTICIPANT.name());
            }
        });

        btnAVBType.setOnClickListener(view -> {
            if (mSession != null) {
                mSession.setLocalParticipantType(ParticipantType.AV_BROADCASTER);
                mBundle.putString(PARTICIPANT_TYPE_KEY, ParticipantType.AV_BROADCASTER.name());
            }
        });

        btnABType.setOnClickListener(view -> {
            if (mSession != null) {
                mSession.setLocalParticipantType(ParticipantType.A_BROADCASTER);
                mBundle.putString(PARTICIPANT_TYPE_KEY, ParticipantType.A_BROADCASTER.name());
            }
        });

        btnViewerType.setOnClickListener(view -> {
            if (mSession != null) {
                mSession.setLocalParticipantType(ParticipantType.VIEWER);
                mBundle.putString(PARTICIPANT_TYPE_KEY, ParticipantType.VIEWER.name());
            }
        });

        btnAdjustVideoQuality.setOnClickListener(v -> {
            if (mSession != null) {
                mSession.adjustResolution(widthSeek.getProgress(), heightSeek.getProgress());
                mSession.adjustFrameRate(frameSeek.getProgress());
            }
        });

        btnSendMessage.setOnClickListener(v -> {
            Editable messageEdt = editMessage.getText();
            Message message = new Message(String.valueOf(System.currentTimeMillis()),
                messageEdt.toString(), mDisplayName, System.currentTimeMillis(), true);

            if (mSession != null) {
                mMessengerAdapter.addMessage(message);
                mSession.sendMessage(messageEdt.toString());
                messageEdt.clear();
                mRecyclerMessages.scrollToPosition(mMessengerAdapter.getItemCount() - 1);
            }
        });

        widthSeek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    int widthProgress = getResolutionProgress(progress);
                    widthSeek.setProgress(widthProgress);
                    txtWidth.setText(String.valueOf(widthProgress));

                    boolean isPortraitOrientation = getResources().getConfiguration()
                        .orientation == Configuration.ORIENTATION_PORTRAIT;
                    int heightProgress = (isPortraitOrientation) ?
                        getResolutionProgress(widthProgress * 4 / 3) :
                        getResolutionProgress(widthProgress * 3 / 4);
                    heightSeek.setProgress(heightProgress);
                    txtHeight.setText(String.valueOf(heightProgress));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        heightSeek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    int heightProgress = getResolutionProgress(progress);
                    heightSeek.setProgress(heightProgress);
                    txtHeight.setText(String.valueOf(heightProgress));

                    boolean isPortraitOrientation = getResources().getConfiguration()
                        .orientation == Configuration.ORIENTATION_PORTRAIT;
                    int widthProgress = (isPortraitOrientation) ?
                        getResolutionProgress(heightProgress * 4 / 3) :
                        getResolutionProgress(heightProgress * 3 / 4);
                    heightSeek.setProgress(widthProgress);
                    txtHeight.setText(String.valueOf(widthProgress));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        frameSeek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    txtFrameRate.setText(String.valueOf(progress));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        // Create PublisherAdapter instance
        mParticipantAdapter = new ParticipantsAdapter(this);
        // Set PublisherAdapter to RecyclerView
        RecyclerView recyclerParticipants = findViewById(R.id.participants);
        recyclerParticipants.setAdapter(mParticipantAdapter);
        boolean isPortraitOrientation = getResources().getConfiguration()
            .orientation == Configuration.ORIENTATION_PORTRAIT;
        if (recyclerParticipants.getLayoutManager() instanceof GridLayoutManager) {
            ((GridLayoutManager) recyclerParticipants.getLayoutManager())
                .setSpanCount(isPortraitOrientation ? 2 : 4);
        }
    }

    private int getResolutionProgress(int progress) {
        if (progress < 144) {
            progress = 120;
        } else if (progress < 160) {
            progress = 144;
        } else if (progress < 176) {
            progress = 160;
        } else if (progress < 240) {
            progress = 176;
        } else if (progress < 288) {
            progress = 240;
        } else if (progress < 320) {
            progress = 288;
        } else if (progress < 352) {
            progress = 320;
        } else if (progress < 360) {
            progress = 352;
        } else if (progress < 480) {
            progress = 360;
        } else if (progress < 600) {
            progress = 480;
        } else if (progress < 640) {
            progress = 600;
        } else if (progress < 720) {
            progress = 640;
        } else if (progress < 768) {
            progress = 720;
        } else if (progress < 800) {
            progress = 768;
        } else if (progress < 1024) {
            progress = 800;
        } else if (progress < 1200) {
            progress = 1024;
        } else if (progress < 1280) {
            progress = 1200;
        } else if (progress < 1440) {
            progress = 1280;
        } else if (progress < 1512) {
            progress = 1440;
        } else if (progress < 1536) {
            progress = 1512;
        } else if (progress < 1600) {
            progress = 1536;
        } else if (progress < 1944) {
            progress = 1600;
        } else if (progress < 2400) {
            progress = 1944;
        } else if (progress < 2448) {
            progress = 2400;
        } else if (progress < 2560) {
            progress = 2448;
        } else if (progress < 3264) {
            progress = 2560;
        } else {
            progress = 3264;
        }
        return progress;
    }

    private void setParticipantsTypesSheet() {
        BottomSheetBehavior.from(mParticipantsTypesSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
        BottomSheetBehavior.from(mParticipantsTypesSheet).setDraggable(true);

        //click event for show-dismiss bottom sheet
        findViewById(R.id.sheet_header).setOnClickListener(view -> {
            if (BottomSheetBehavior.from(mParticipantsTypesSheet).getState()
                != BottomSheetBehavior.STATE_EXPANDED) {
                BottomSheetBehavior.from(mParticipantsTypesSheet)
                    .setState(BottomSheetBehavior.STATE_EXPANDED);
            } else {
                BottomSheetBehavior.from(mParticipantsTypesSheet)
                    .setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });
    }

    private void initSession() {
        mSession = new Session.SessionBuilder(this)
            .setReconnectListener(this)
            .setConnectionListener(this)
            .build(this);
    }

    private void setSessionParams() {
        mSession.setDisplayName(mDisplayName);
    }

    @Override
    protected void onPause() {
        disconnect();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Check if Session was disconnected because of app was putted into background
        if (mBundle.getBoolean(SESSION_CONNECTION_KEY, false)) {
            // Clear connection session value in the default state
            mBundle.putBoolean(SESSION_CONNECTION_KEY, false);
            // init  Session again as Session was cleared when app was putting into background
            initSession();
            // resume Session connection with previous participant's type, default is full participant
            mSession.connect(mToken, ParticipantType
                .valueOf(mBundle.getString(PARTICIPANT_TYPE_KEY, "FULL_PARTICIPANT")));
        }
    }

    @Override
    public void onBackPressed() {
        if (BottomSheetBehavior.from(mParticipantsTypesSheet).getState() == BottomSheetBehavior.STATE_EXPANDED) {
            BottomSheetBehavior.from(mParticipantsTypesSheet).setState(BottomSheetBehavior.STATE_COLLAPSED);
            return;
        }
        // If its onBackPressed, we do not suppose resuming connection. so clear connection session value in the default state
        mBundle.putBoolean(SESSION_CONNECTION_KEY, false);
        super.onBackPressed();
    }

    private void disconnect() {
        clearAdapters();
        if (mSession != null) {
            mSession.disconnect();
            mSession = null;
        }
    }

    private void clearAdapters() {
        // Clear participants and disconnect from the session
        if (mParticipantAdapter != null) {
            mParticipantAdapter.clearPublishers();
        }
    }

    @Override
    public void onSwitchCameraClicked() {
        if (mSession != null) {
            mSession.switchCamera();
        }
    }

    @Override
    public void onConnected(@NonNull List<? extends Participant> list) {
        // Save connection state of the Session
        mBundle.putBoolean(SESSION_CONNECTION_KEY, true);
        findViewById(R.id.type_container).setVisibility(View.VISIBLE);
        findViewById(R.id.txt_session_time_duration).setVisibility(View.VISIBLE);
        findViewById(R.id.message_container).setVisibility(View.VISIBLE);
        Utill.showMessage(this, "Connected to session");
        long connectionStarted = System.currentTimeMillis();
        mCountTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (isFinishing()) {
                    cancel();
                    return;
                }
                runOnUiThread(() -> ((AppCompatTextView) findViewById(R.id.txt_session_time_duration))
                    .setText(Utill.formatDurationToHHMMSSSSS(System.currentTimeMillis() - connectionStarted)));
            }
        }, 0, 1000);
    }

    @Override
    public void onError(SessionError error) {
        switch (error.getCode()) {
            case 101:
            case 106:
                if (mSession != null) {
                    mSession.disconnect();
                }
                break;
            default:
                break;
        }
        Utill.showError(this, error.getMessage());
    }

    @Override
    public void onForceDisconnected(@NonNull String s, @Nullable String s1) {
        Utill.showMessage(this, "force disconnect Participant with message $message");
        onDisconnected();
        finish();
    }

    @Override
    public void onDisconnected() {
        Utill.showError(this, "Disconnected from session");
        if (mSession != null) {
            mSession.resetLocalAudioVideoState();
        }
        finish();
    }

    @Override
    public void onLocalParticipantJoined(@Nullable Participant participant) {
        if (mParticipantAdapter != null) {
            mParticipantAdapter.addLocalParticipant(participant);
        }
    }

    @Override
    public void onRemoteParticipantJoined(@Nullable Participant participant) {
        if (mParticipantAdapter != null) {
            mParticipantAdapter.addRemoteParticipant(participant);
            // You can set RemoteParticipant's volume level after adding participant's object to adapter
            if (participant != null) {
                participant.setVolumeLevel(1);
            }
        }
    }

    @Override
    public void onMessageReceived(@NotNull String participantId, @NotNull String message) {
        if (mParticipantAdapter != null) {
            String participantName = mParticipantAdapter.getParticipantName(participantId);

            Log.i("MESSAGE", "Got message from participant with id->" + participantId +
                " and name->" + participantName+ ", message->" + message);

            if (mSession != null) {
                mMessengerAdapter.addMessage(new Message(String.valueOf(System.currentTimeMillis()),
                    message, mDisplayName, System.currentTimeMillis(), false));
                mRecyclerMessages.scrollToPosition(mMessengerAdapter.getItemCount() - 1);
            }
        }
    }

    @Override
    public void onUpdateParticipant(@Nullable String participantId, @Nullable Participant participant) {
        if (mParticipantAdapter != null) {
            mParticipantAdapter.updateParticipant(participantId, participant);
        }
    }

    @Override
    public void onRemoteParticipantLeft(@Nullable String participantId) {
        if (mParticipantAdapter != null) {
            mParticipantAdapter.removeParticipant(participantId);
        }
    }

    @Override
    public void onParticipantMediaStateChanged(@Nullable String participantId, MediaType mediaType,
        MediaState mediaState) {
        if (mParticipantAdapter != null) {
            mParticipantAdapter.updateParticipantMedia(participantId, mediaType, mediaState);
        }
    }

    /**
     * SessionReconnectListener's callbacks
     *
     * */
    @Override
    public void onParticipantReconnecting(@Nullable String participantId) {
        mParticipantAdapter.progressConnection(participantId, true);
        if (mReconnectTimer != null) {
            mReconnectTimer.cancel();
        }
        mReconnectTimer = new Timer("MainActivityReconnectThread");
        mReconnectTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(() -> {
                    if (mParticipantAdapter != null) {
                        mParticipantAdapter.removeParticipant(participantId);
                    }
                });
            }
        }, 7000);
    }

    @Override
    public void onParticipantReconnected(@Nullable String participantId, @Nullable String s1) {
        if (mReconnectTimer != null) {
            mReconnectTimer.cancel();
        }
        if (isFinishing()) {
            return;
        }
        if (mParticipantAdapter != null) {
            mParticipantAdapter.progressConnection(participantId, false);
        }
    }

    /**
     * SessionConnectionListener's callbacks
     *
     * */
    @Override
    public void onLocalConnectionLost() {
        setConnectionState(R.string.txt_connection_lost, false);
    }

    @Override
    public void onLocalConnectionResumed() {
        setConnectionState(R.string.txt_connection_resumed, true);
    }

    @Override
    public void onRemoteConnectionLost(@Nullable String participantId) {
        if (isFinishing()) {
            return;
        }
        if (mParticipantAdapter != null) {
            mParticipantAdapter.remoteParticipantConnectionLost(participantId);
        }
        if (mReconnectTimer != null) {
            mReconnectTimer.cancel();
        }
        mReconnectTimer = new Timer("MainActivityReconnectThread");
        mReconnectTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(() -> {
                    if (mParticipantAdapter != null) {
                        mParticipantAdapter.removeParticipant(participantId);
                    }
                });
            }
        }, 20000);
    }

    private void setConnectionState(int strConnectionState, boolean isConnected) {
        int txtColor = isConnected ? R.color.colorGreen : R.color.colorRed;
        mConnectionState.setVisibility(View.VISIBLE);
        mConnectionState.setText(strConnectionState);
        mConnectionState.setBackgroundColor(ContextCompat.getColor(this, txtColor));
        mConnectionStateTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(() -> mConnectionState.setVisibility(View.GONE));
            }
        }, 5000);
    }
}